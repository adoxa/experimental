from Plugins.Plugin import PluginDescriptor
from Components.About import about
from Screens.SoftwareUpdate import UpdatePlugin
from Components.Ipkg import IpkgComponent

PACKAGE = 'enigma2-plugin-systemplugins-experimental'
getBuildString_org = about.getBuildString
startActualUpgrade_org = UpdatePlugin.startActualUpgrade
showUpdateCompletedMessage_org = UpdatePlugin.showUpdateCompletedMessage

def getBuildString_new():
	return getBuildString_org() + " (experimental)"

def startActualUpgrade_new(self, answer):
	if answer and answer[1] == "hot":
		self.notdoneyet = True
		self.processed_packages.append(PACKAGE)
		self.ipkg.startCmd(IpkgComponent.CMD_REMOVE, {'package': PACKAGE})
	else:
		startActualUpgrade_org(self, answer)

def showUpdateCompletedMessage_new(self):
	if hasattr(self, 'notdoneyet'):
		del self.notdoneyet
		startActualUpgrade_org(self, (None, "hot"))
	else:
		showUpdateCompletedMessage_org(self)

def autostart(reason, **kwargs):
	if reason == 0: # startup
		about.getBuildString = getBuildString_new
		UpdatePlugin.startActualUpgrade = startActualUpgrade_new
		UpdatePlugin.showUpdateCompletedMessage = showUpdateCompletedMessage_new
	elif reason == 1: # shutdown
		about.getBuildString = getBuildString_org
		UpdatePlugin.startActualUpgrade = startActualUpgrade_org
		UpdatePlugin.showUpdateCompletedMessage = showUpdateCompletedMessage_org

def Plugins(**kwargs):
	return PluginDescriptor(where=[PluginDescriptor.WHERE_AUTOSTART], fnc=autostart, needsRestart=True)
